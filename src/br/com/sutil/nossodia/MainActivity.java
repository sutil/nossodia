package br.com.sutil.nossodia;

import java.util.Calendar;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends Activity {

	TextView text;
	Thread t;
	boolean valendo = true;
	Calendar casa;
	Calendar hoje;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		text = (TextView) findViewById(R.id.text);
		casa = Calendar.getInstance();
		casa.set(2014, 8, 6, 17, 0, 0);
		new Contador().execute("");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(1, 1, 0, "Compartilhar").setIcon(
				android.R.drawable.ic_menu_share);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == 1) {
			compartilhar();
		}
		return super.onOptionsItemSelected(item);
	}

	private void escreveNaTela() {
		long[] values = calculaDias();
		String str;
		if (values[0] > 0) {
			StringBuilder sb = new StringBuilder();
			sb.append("Faltam ");
			sb.append(values[0] + " dias, ");
			sb.append(values[1] + " horas, ");
			sb.append(values[2] + " minutos e ");
			sb.append(values[3] + " segundos ");
			sb.append("para o nosso casamento");
			str = sb.toString();
		} else if (values[0] == 0) {
			str = "Hoje é o grande dia!";
		} else {
			str = "Estamos casados há " + (values[0] * (-1)) + " dias!";
		}
		text.setText(str);
	}

	private long[] calculaDias() {
		hoje = Calendar.getInstance();
		long diferenca = casa.getTimeInMillis() - hoje.getTimeInMillis();
		long dias = diferenca / (1000 * 60 * 60 * 24);
		long horas = (diferenca / (1000 * 60 * 60)) % 24;
		long minutos = (diferenca / (1000 * 60)) % 60;
		long segundos = (diferenca / 1000) % 60;
		return new long[] { dias, horas, minutos, segundos };
	}

	@Override
	public void onBackPressed() {
		valendo = false;
		super.onBackPressed();
	}

	class Contador extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			while (valendo) {
				try {
					Thread.sleep(990);
					publishProgress(1);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			escreveNaTela();
			super.onProgressUpdate(values);
		}

	}
	
	private void compartilhar(){
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("text/plain");
		i.putExtra(Intent.EXTRA_TEXT, text.getText().toString());
		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
		startActivity(i);
	}


}
